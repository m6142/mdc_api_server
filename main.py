from flask import Flask, Blueprint, jsonify, request
from flask_sqlalchemy import SQLAlchemy
import sqlite3
from datetime import datetime
from log import Log, log
import re

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///data.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

class Data(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    ammonia = db.Column(db.Float(precision=2, asdecimal=True), nullable=False)
    do = db.Column(db.Float(precision=2, asdecimal=True), nullable=False)
    ph = db.Column(db.Float(precision=2, asdecimal=True), nullable=False)
    date_added = db.Column(db.String(20), nullable=False)

    def __repr__(self):
        a = '{0:.2f}'.format(self.ammonia)
        d = '{0:.2f}'.format(self.do)
        p = '{0:.2f}'.format(self.ph)
        return f"{self.id}| {a}, {d}, {p} | {self.date_added}"
    
    def serialize(self):
        return {
            'id': int(self.id),
            'ammonia': float(self.ammonia),
            'do': float(self.do),
            'ph': float(self.ph),
            'date_added': self.date_added
        }




def check_values(ammonia: float, do: float, ph: float) -> bool:
    """
        ### check_values
        check all the sensor value if they are in correct range and not some random value\n\n
        args:
            * ammonia: float - ammonia value
            * do: float - dissolved oxygen value
            * ph: float - pH value\n
        return:
            returns True if all the values are appropriate otherwise False
    """
    if(ammonia != None and do != None and ph != None):
        if not (0 < ph < 14):
            log(Log.ERR, "make sure 0 < ph < 14")
            return False
        elif not (0 < do < 20):
            log(Log.ERR, "make sure 0 < D.O. < 20")
            return False
        elif not (0 < ammonia < 1):
            log(Log.ERR, "make sure 0 < ammonia < 1")
            return False
        else:
            return True
    else:
        return False


def check_date_format(date: str) -> bool:
    """
        ### check_date
        check if the date is in correct format inside the string\n\n
        args:
            date: str - date in string format\n
        return:
            returns True if date is in correct format otherwise False

    """
    pattern = r"[\d]{4}-[\d]{2}-[\d]{2} [\d]{2}:[\d]{2}:[\d]{2}"
    match = re.fullmatch(re.compile(pattern), date)
    if(match != None):
        return True
    else:
        log(Log.ERR, "incorrect date format")
        return False


def add_data(ammonia: float, do: float, ph: float, date_added: str) -> int:
    """
        ### add_data
        adds new data with the parameters\n\n
        args:
            * ammonia: float - ammonia value
            * do: float - dissolved oxygen value
            * ph: float - pH value
            * date_added: str - date added in string format\n
        return:
            returns 0 is success otherwise 1
    """
    if check_values(ammonia, do, ph) and check_date_format(date_added):
        data = Data(ammonia=ammonia, do=do, ph=ph, date_added=date_added)
        db.session.add(data)
        db.session.commit()
        return 0
    else:
        return 1

def add_data_autodate(ammonia: float, do: float, ph: float) -> int:
    """
        ### add_data
        adds new data with the parameters and automatic date\n\n
        args:
            * ammonia: float - ammonia value
            * do: float - dissolved oxygen value
            * ph: float - pH value
            * date_added: str - date added in string format\n
        return:
            returns 0 is success otherwise 1
    """
    date = datetime.now()
    dateStr = date.strftime("%Y-%m-%d %H:%M:%S")
    if check_values(ammonia, do, ph):
        data = Data(ammonia=ammonia, do=do, ph=ph, date_added=dateStr)
        db.session.add(data)
        db.session.commit()
        return 0
    else:
        return 1

def get_data(id: int) -> Data:
    """
        ### get_data
        get single data with id and returns it\n\n
        args:
            id: int - id of the required data\n
        return:
            returns Data if exists otherwise None
    """
    data = Data.query.filter_by(id=id).first()
    if data:
        return data.serialize()
    else:
        log(Log.ERR, "make sure id already exists")
        return None

def get_all() -> list[Data]:
    """
        ### get_all
        get all data and return the list\n\n
        return:
            return a  list of Data otherwise None
    """
    datas = Data.query.all()
    if datas:
        return [data.serialize() for data in datas]
    else:
        log(Log.LOG, "the list is empty")
        return None

def update_data(id: int, ammonia: float, do: float, ph: float, date_added: str) -> int:
    """
        ### update_data
        update data with id with the new parameters\n\n
        args:
            * id: int - id of the data to be changed
            * ammonia: float - new ammonia value
            * do: float - new dissolved oxygen value
            * ph: float - new pH value
            * date_added - date of making the update\n
        return:
            return 0 is update success, 1 if incorrect data provided, 2 if id doesn't exists
    """
    data = Data.query.filter_by(id=id).first()
    if data:
        if check_values(ammonia, do, ph) and check_date_format(date_added):
            data.ammonia = ammonia
            data.do = do
            data.ph = ph
            data.date_added = date_added
            db.session.add(data)
            db.session.commit()
            return 0
        else:
            return 1
    else:
        log(Log.ERR, "the passed id does not exists.")
        return 2

def update_data_autodate(id: int, ammonia: float, do: float, ph: float) -> int:
    """
        ### update_data
        update data with id with the new parameters and automatic date\n\n
        args:
            * id: int - id of the data to be changed
            * ammonia: float - new ammonia value
            * do: float - new dissolved oxygen value
            * ph: float - new pH value
            * date_added - date of making the update\n
        return:
            return 0 is update success otherwise 1
    """
    date = datetime.now()
    dateStr = date.strftime("%Y-%m-%d %H:%M:%S")
    data = Data.query.filter_by(id=id).first()
    if data:
        if check_values(ammonia, do, ph):
            data.ammonia = ammonia
            data.do = do
            data.ph = ph
            data.date_added = dateStr
            db.session.add(data)
            db.session.commit()
            return 0
        else:
            return 1
    else:
        log(Log.ERR, "the given id doesn't exist.")
        return 2

def delete_data(id: int) -> int:
    """
        ### delete_data
        delete the data with given id\n\n
        args:
            id: int - id of the data to be deleted\n
        return:
            returns 0 is deletion success otherwise 1
    """
    data = Data.query.filter_by(id=id).first()
    if data:
        db.session.delete(data)
        db.session.commit()
        return 0
    else:
        log(Log.ERR, "the given id doesn't exist.")
        return 1

# check its implementation later
def delete_all() -> int:
    """
        ### delete_all
        delete all the data\n\n
        return:
            returns 0 if deletion success otherwise 1
    """
    con = sqlite3.connect('data.db')
    cur = con.cursor()
    cur.execute("ALTER TABLE data RENAME TO data_backup;")
    con.commit()
    con.close()

    db.create_all()


api = Blueprint('api', __name__)

@api.route('/')
def api_home():
    return jsonify({'msg': 'this is Manual Data Collection API...'})

@api.route('/sensor_values')
def getall_sensor_values():
    datas = get_all()
    return jsonify(datas)

@api.route('/sensor_values/<int:id>')
def get_sensor_value(id: int):
    data = get_data(id)
    return jsonify(data)

@api.route('/sensor_values', methods=['POST'])
def add_sensor_values():
    if('application/json' in request.headers['Content-Type']):
        # print(request.json)
        if(request.json):
            # add CHECKS if these keys are passed or not
            ammonia = request.json.get('ammonia')
            do = request.json.get('do')
            ph = request.json.get('ph')
        else:
            return jsonify({'status': 1, 'msg': 'send the json values along with request.'})
    else:
        return jsonify({'status': 1, 'msg': 'set Content-Type to application/json in headers.'})

    status = add_data_autodate(ammonia, do, ph)
    return jsonify({'status': status})

@api.route('/sensor_values/<int:id>', methods=['PUT'])
def update_sensor_values(id: int):
    if('application/json' in request.headers['Content-Type']):
        # print(request.json)
        if(request.json):
            # add CHECKS if these keys are passed or not
            ammonia = request.json.get('ammonia')
            do = request.json.get('do')
            ph = request.json.get('ph')
        else:
            return jsonify({'status': 1, 'msg': 'send the json values along with request.'})
    else:
        return jsonify({'status': 1, 'msg': 'set Content-Type to application/json in headers.'})

    status = update_data_autodate(id, ammonia, do, ph)
    return jsonify({'status': status})

@api.route('/sensor_values/<int:id>', methods=['DELETE'])
def delete_sensor_values(id: int):
    status = delete_data(id)
    return jsonify({'status': status})

app.register_blueprint(api, url_prefix="/api")

@app.route("/")
def home():
    return "<p>Home Page</p>"


if __name__ == "__main__":
    print("Manual Data Collection")
    print("-" * 12)
    db.create_all()


    # TEST FUNCTIONS

    # add_data(0.2, 7.2, 8, "2021-10-28 17:28:50")
    # add_data_autodate(0.4, 3.2, 9)
    # add_data_autodate(0.55, 6.2, 11)

    # update_data(1, 0.20, 7.9, 8, "2021-10-28 17:28:51")
    # update_data_autodate(3, 0.20, 7.9, 8)

    # delete_data(3)

    # print(get_data(2))

    # data = get_all()
    # if(data != None):
    #     for val in data:
    #         print(val)

    app.run(debug=True, host='0.0.0.0', port=5000)