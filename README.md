# Manual Data Collection API Server

Python flask api server with sqlite database.

Setup
> Windows
```powershell
python -m venv venv
venv/Scripts/Activate.ps1
pip install -r requirements.txt

python main.py
```
